# Jornaya-Platform-MVP-Automation

## Description

Repository for automated test cases on Jornaya-Platform-MVP project.

Built and tested with Python 3.7

## Pre Requisites

* Go 3.7+

## License

Check the [LICENSE](LICENSE) file for details.
