# @author Ariannis Abella

Feature: SignIn

  Background: Sign-In using valid credentials
    Given Load the Url

    Scenario Outline: Sign-In using valid credentials
      When Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username>"
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password>"
      And Click button with XPATH "---Insert Xpath Here---"
      And Wait:"10" seconds until the URL changes to a new URL
        Then Should redirect to /--InsertValue--


 Examples: DATA: Valid Username and Password
      | Username | Password |
      |          |          |
