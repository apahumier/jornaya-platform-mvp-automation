# @author Ariannis Abella

Feature: SignIn

  Background: Sign-In using invalid credentials (Username and Password)
    Given Load the Url

    Scenario Outline: Sign-In using invalid Username and valid Password
       When Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username>"
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password>"
       And Click button with XPATH "---Insert Xpath Here---"
       And the user will see a message "<Warning_error>" in this Id "errorMessage"
         Then Should redirect to /--InsertValue--


         Examples: DATA: Invalid Username and valid Password
              | Username | Password | Warning_error|
              | invalid  |  valid   |     error    |


   Scenario Outline: Sign-In using invalid Password and valid Username
      When Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username>"
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password>"
      And Click button with XPATH "---Insert Xpath Here---"
      And the user will see a message "<Warning_error>" in this Id "errorMessage"
        Then Should redirect to /--InsertValue--


         Examples: DATA: Invalid Password and valid Username
              | Username | Password | Warning_error|
              |    valid | invalid  |   error      |


   Scenario Outline: Sign-In using invalid credentials (Username and Password)
      When Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username>"
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password>"
      And Click button with XPATH "---Insert Xpath Here---"
      And the user will see a message "<Warning_error>" in this Id "errorMessage"
        Then Should redirect to /--InsertValue--


         Examples: DATA: Invalid Username and Password
              | Username | Password | Warning_error|
              |  invalid | invalid  |   error      |