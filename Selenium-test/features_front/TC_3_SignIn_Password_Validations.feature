# @author Ariannis Abella

Feature: SignIn

  Background: Sign-In password validations
    Given Load the Url

    Scenario Outline: Sign-In password validations : ""
      When Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username>"
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password>"
      And Click button with XPATH "---Insert Xpath Here---"
      And Wait:"10" seconds until the URL changes to a new URL
      And the user will see a message "<Warning_error>" in this Id "errorMessage"


      Examples: DATA: Valid Username and Password
        | Username |  Password | Warning_error |
        | valid    | invalid   |       error   |





