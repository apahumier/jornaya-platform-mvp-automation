# @author Ariannis Abella

Feature: Create a new integration (Salesforce - DP_Jornaya)

  Background: Create a new integration
    Given Load the Url

    Scenario Outline: Precondition - Sign-In using valid credentials
      When Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username>"
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password>"
      And Click button with XPATH "---Insert Xpath Here---"
      And Wait:"10" seconds until the URL changes to a new URL
        Then Should redirect to /--InsertValue--

#     Step1- Activate
      When Click button with XPATH "---Insert Xpath Here---"
      And Wait:"10" seconds until the URL changes to a new URL
        Then Should redirect to /--InsertValue--

#     Step2- Add Integrations Salesforce - Deliver Portafolio to Jornaya
      When Click button with XPATH "---Insert Xpath Here---"
      And Wait until element with Xpath:"---Insert Xpath Here---" is available
      And Click button with XPATH "---Insert Xpath Here---"
      And Wait until element with Xpath:"---Insert Xpath Here---" is available
      And Click button with XPATH "---Insert Xpath Here---"

#     Step3- New Authentication
      And Wait until element with Xpath:"---Insert Xpath Here---" is available
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Authentication_Name>"
      And Click Drop-Down list with Xpath "---Insert Xpath Here---" and Value with Xpath "---Insert Xpath Here---"
      And Click button with XPATH "---Insert Xpath Here---"

#      Step4 - Salesforce login
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username_1>"
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password_1>"
      And Click button with XPATH "---Insert Xpath Here---"
      And Wait 5 seconds
      And Click button with XPATH "---Insert Xpath Here---"

#      Step5- new account
#      Basic Info
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Authentication_name>"
#      Transfer Info
       And Click Drop-Down list with Xpath "---Insert Xpath Here---" and Value with Xpath "---Insert Xpath Here---"
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Port>"
       And Click Drop-Down list with Xpath "---Insert Xpath Here---" and Value with Xpath "---Insert Xpath Here---"
#      FTP Log-in
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username_2>"
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password_2>"
#      Security
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Private_Key>"
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Pass_Phrase>"
       And Click button with XPATH "---Insert Xpath Here---"
       And Click button with XPATH "---Insert Xpath Here---"
#      Integration Details
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username_2>"
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Number_ORTM>"
       And "create a new function to make click radio-button"
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<SalesForce_F>"
       And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Jornaya_F>"
       And Click button with XPATH "---Insert Xpath Here---"
         Then Should redirect to /--InsertValue--
#       deberia validar aqui que en el campo "authentication" que el correo sea el mismo que agregue antes ..

         Examples: DATA
              | Username | Password |Authentication_Name|Username_1|Password_1|Authentication_name|Port|
              |          |          |                   |          |          |                   |    |

         Examples: DATA
              |Username_1|Password_2|Private_Key|Pass_Phrase|Number_ORTM|SalesForce_F|Jornaya_F|
              |          |          |           |           |           |            |         |

