# @author Ariannis Abella

Feature: Edit Integration

  Background: Edit Integration
    Given Load the Url

    Scenario Outline: Precondition - Sign-In using valid credentials and should be on Integrations screen
      When Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Username>"
      And Fill the field with Xpath:"---Insert Xpath Here---" and value:"<Password>"
      And Click button with XPATH "---Insert Xpath Here---"
      And Wait:"10" seconds until the URL changes to a new URL
        Then Should redirect to /--InsertValue--

#     Step1- Activate
      When Click button with XPATH "---Insert Xpath Here---"
      And Wait:"10" seconds until the URL changes to a new URL
        Then Should redirect to /--InsertValue--

#     Step2- Edit Integrations
      When Click button with XPATH "---Insert Xpath Here---"
      And Wait until element with Xpath:"---Insert Xpath Here---" is available
      And Click button with XPATH "---Insert Xpath Here---"
       Then Should redirect to /--InsertValue--


#     Step3- Edit data
      When Click button with XPATH "---Insert Xpath Here---"
      And Edit Authentication Name
      And Edit port
      And Edit Cipher
      And Click button with XPATH "---Insert Xpath Here---"
      And Click button with XPATH "---Insert Xpath Here---"

#       deberia validar aqui que en el campo "authentication" que el correo sea el mismo que agregue antes ..

         Examples: DATA
              | Username | Password |
              |          |          |



