# Behave has an interesting page/heap structure behind the scenes
# that obfuscates what is and isn't in the context object.
# This class is instantiated in the before_all function and used
# to assign values, so that they persist across scenario tests

# from tests.config import *

BEHAVE_DEBUG_ON_ERROR = False

def after_step(context, step):
    if step.status == "failed":
        file_name = step.filename
        scenario_name = context.scenario_name
        step_name = step.name
        content = str('**Behave Test**\n'+'```File: '+ file_name +'\nScenario: '+scenario_name+'\nStep FAILE: '+step_name+'```')


def before_scenario(context, scenario):
    # print(scenario.name)
    context.scenario_name = scenario.name



def after_feature(context, feature):
    name = feature.name
    if feature.status == "passed":
        content = str('**Behave Test**\n'+'```'+'feature:'+name+'\nAll tests were completed successfully'+'```')
        print(feature.status)
