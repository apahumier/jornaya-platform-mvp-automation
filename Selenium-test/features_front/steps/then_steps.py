from behave import then
from hamcrest import assert_that, equal_to, is_not
from selenium.webdriver.support import expected_conditions as EC
from features_front import routes
import time

@then("Title is {text}")
def step_impl(context, text):
    assert_that(context.driver.title, equal_to(text))


@then("Should redirect to {path}")
def step_impl(context, path):
    context.base_url = routes.route.url_base
    # assert_that(context.driver.current_url, equal_to(context.base_url + path))
    print(path)
    assert_that(context.driver.current_url, equal_to(context.base_url + path))

    time.sleep(4)

