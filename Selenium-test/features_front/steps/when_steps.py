from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from behave import when
import time
from hamcrest import assert_that, equal_to
import random
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver


# -----------------------------**** EXPLICIT WAIT **** ----------------------------
# Wait with Id
@when('Wait until element with Id:"{Id}" is available')
def step_impl(context,Id):
  try:
      WebDriverWait(context.driver, 5).until(EC.presence_of_all_elements_located((By.ID, Id)))
      print("Find it!")
  except TimeoutException:
      print("Too much time to find this element!")

# Wait with Xpath
@when('Wait until element with Xpath:"{Xpath}" is available')
def step_impl(context,Xpath):
  try:
      WebDriverWait(context.driver, 5).until(EC.presence_of_all_elements_located((By.XPATH, Xpath)))
      print("Find it!")
  except TimeoutException:
      print("Too much time to find this element!")

# Wait with CssSelector
@when('Wait until element with Css:"{CssSelector}" is available')
def step_impl(context,CssSelector):
  try:
      WebDriverWait(context.driver, 5).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, CssSelector)))
      print("Find it!")
  except TimeoutException:
      print("Too much time to find this element!")


@when('Wait:"{seconds}" seconds until element with Id:"{Id}" contains the text:"{text}"')
def step_impl(context, seconds, Id, text):
    wait = WebDriverWait(context.driver, int(seconds))
    wait.until(EC.text_to_be_present_in_element((By.ID, Id), text))


@when('Wait:"{seconds}" seconds until the URL changes to a new URL')
def step_impl(context, seconds, ):
    actual = context.driver.current_url
    wait = WebDriverWait(context.driver, int(seconds))
    try:
        wait.until(lambda driver: actual != context.driver.current_url)
    except:
        assert_that(context.driver.current_url != context.driver.current_url)


@when('Wait:"{seconds}" seconds until all elements inside the container with class:"{className}" are available')
def step_impl(context, seconds, className):
    wait = WebDriverWait(context.driver, int(seconds))
    wait.until(EC.visibility_of_any_elements_located((By.CLASS_NAME, className)))
    time.sleep(1)


@when('Wait:"{seconds}" seconds until element with Id:"ID"')
def step_impl(context, seconds, ID):
    try:
        WebDriverWait(context.driver, int(seconds)).until(EC.presence_of_element_located(context.driver.find_element_by_id(ID)))
        print ("Page is ready!")
    except TimeoutException:
        print ("Loading took too much time!")

# @when('Wait "{seconds}" second until the element with ID "{Idname}" is seen')
# def step_impl(context, seconds,Idname):
#     context.driver.implicitly_wait(int(seconds))    # seconds
#     context.driver.find_element_by_id(Idname)
#
#
# @when('wait "{seconds}" second until find the item with ID:"{Idname}"')
# def step_impl(context, seconds,Idname):
#     context.driver.implicitly_wait(int(seconds))    # seconds
#     context.driver.find_element_by_id(Idname)

# ---------------------------------**** IMPLICIT WAIT **** ----------------------------
@when('Wait 10 seconds')
def step_impl(context):
    context.field = 'wait'
    time.sleep(10)

@when('Wait 5 seconds')
def implicitly_wait(context):
        context.field = 'wait'
        time.sleep(5)

@when('Wait:"{numbers}" seconds')
def step_impl(context, numbers):
    context.field = 'wait'
    time.sleep(int(numbers))

# ---------------------------------**** CLICK BUTTON ****-------------------------------------
@when('Click button with ID "{IDbutton}"')
def step_impl(context, IDbutton):
    wait = WebDriverWait(context.driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.ID, IDbutton)))
    element.click()

@when('Click button with Name "{NameButton}"')
def step_impl(context, NameButton):
    wait = WebDriverWait(context.driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.NAME, NameButton)))
    element.click()
    # context.driver.implicitly_wait(10)    # seconds
    # context.driver.find_element_by_name(NameButton).click()
    # # time.sleep(2)

@when('Click button with XPATH "{XPATH}"')
def step_impl(context, XPATH):
    wait = WebDriverWait(context.driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.XPATH, XPATH)))
    element.click()
    # context.driver.implicitly_wait(10)    # seconds
    # context.driver.find_element_by_xpath(XPATH).click()
    # # time.sleep(10)


@when('Click button with Class name "{Class}"')
def step_impl(context, Class):
    wait = WebDriverWait(context.driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, Class)))
    element.click()
    # context.driver.implicitly_wait(10)    # seconds
    # context.driver.find_element_by_class_name(Class).click()
    # # time.sleep(5)


@when('Click button with CCS "{selector}"')
def step_impl(context, selector):
    wait = WebDriverWait(context.driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))
    element.click()
    # context.driver.find_element_by_css_selector(selector).click()
    # time.sleep(5)


@when('Click CCS "{selector}"')
def step_impl(context, selector):
    context.driver.find_element_by_class_name(selector).click()

# #Special Action
# @when('Click on EDIT')
# def step_impl(context):
#     context.driver.find_element_by_css_selector('.items-stretch :nth-child(2) .my-card i').click()
#     time.sleep(2)
#     context.driver.find_element_by_xpath('/html/body/div[3]/div/div[1]').click()
#     time.sleep(2)

# ----------------------------**** CLICK CHECK_BUTTON ****-------------------------------------
@when('Click on checkbox with Xpath "{Xpath}"')
def step_impl(context, Xpath):
        element = context.driver.find_element_by_xpath(Xpath)
        print(element)
        element.click()

@when('Click on checkbox with Id "{Id}"')
def step_impl(context, Id):
        element = context.driver.find_element_by_id(Id)
        print(element)
        element.click()


# ----------------------------**** CLICK DROP-DOWN ****-------------------------------------

@when('Click Drop-Down list with ID "{Id}" and Value CSS "{ByCss}"')
def step_impl(context, Id, ByCss):
    context.driver.implicitly_wait(5)  # seconds
    context.driver.find_element_by_id(Id).click()
    context.driver.implicitly_wait(5)  # seconds
    context.driver.find_element_by_css_selector(ByCss).click()
    time.sleep(2)

@when('Click Drop-Down list with Xpath "{Xpath}" and Value with Xpath "{Xpath1}"')
def step_impl(context, Xpath, Xpath1):
    context.driver.implicitly_wait(5)  # seconds
    context.driver.find_element_by_xpath(Xpath).click()
    context.driver.implicitly_wait(5)  # seconds
    context.driver.find_element_by_xpath(Xpath1).click()
    time.sleep(2)


# -----------------------------------------**** FILL ****--------------------------------------
@when('Fill the field with Xpath:"{FieldXpath}" and value:"{value}"')
def step_impl(context, FieldXpath, value):
    context.driver.implicitly_wait(10)  # seconds
    context.driver.find_element_by_xpath(FieldXpath).send_keys(value)
   # time.sleep(2)

@when('Fill the field with Id "{IdValue}" and value "{value}"')
def step_impl(context, IdValue, value):
    context.driver.implicitly_wait(10)
    context.driver.find_element_by_id(IdValue).send_keys(value)


@when('Fill the field with Css Selector "{Selector}" and value "{value}"')
def step_impl(context, Selector, value):
    context.driver.implicitly_wait(10)  # seconds
    context.driver.find_element_by_css_selector(Selector).send_keys(value)


@when('Fill the field with Name "{Fieldname}" and value "{value}"')
def step_impl(context, Fieldname, value):
    context.driver.implicitly_wait(10)  # seconds
    context.driver.find_element_by_name(Fieldname).send_keys(value)
   # time.sleep(2)

# -------------------------------------**** FILL RANDOM ****--------------------------------------
# example = "a"
# # ------------------
# @when('Fill-Random the field with Xpath "{Xpath}"-example')
# def step_impl(context, Xpath):
#     context.driver.implicitly_wait(10)  # seconds  str(random.randrange(4000)
#     # random_var = str(random.randrange(4000))
#     context.driver.find_element_by_xpath(Xpath).send_keys('New' + '12345')
#     example = webdriver.Chrome.find_element_by_xpath(By.XPATH, Xpath).text
#     # example = context.driver.find_element_by_xpath(Xpath).getAttribute("value")
#     print("este es el valor del nombre"+ example)
#     # print(context.random_var)
#    # time.sleep(2)


@when('Fill-Random the field with Xpath "{Xpath}"')
def step_impl(context, Xpath):
    context.driver.implicitly_wait(10)  # seconds
    # random_var = str(random.randrange(4000))
    context.driver.find_element_by_xpath(Xpath).send_keys('New' + str(random.randrange(4000)))
    # print(context.random_var)
   # time.sleep(2)

@when('Fill-Random the field with Id "{Fieldname}" and value "{value}"')
def step_impl(context, Fieldname, value):
    context.driver.implicitly_wait(10)  # seconds
    context.driver.find_element_by_id(Fieldname).send_keys('New' + str(random.randrange(4000)) + value)
   # time.sleep(2)


@when('Fill-Random the field with CSS "{Fieldname}" and value "{value}"')
def step_impl(context, Fieldname, value):
    context.driver.implicitly_wait(10)  # seconds
    context.driver.find_element_by_css_selector(Fieldname).send_keys('New' + str(random.randrange(4000)) + value)


# @when('Fill-Random the field with ID "{Fieldname}"')
# def step_impl(context, Fieldname):
#         context.driver.find_element_by_id(Fieldname).send_keys(Data.Search())

# -------------------------------------**** CLEAR FILL ****--------------------------------------

@when('Clear field by CssSelector:"{field}"')
def step_impl(context, field):
    wait = WebDriverWait(context.driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, field)))
    element.click()
    time.sleep(1)
    size = element.get_attribute('value')
    for i in size:
        element.send_keys(Keys.BACK_SPACE)
    time.sleep(1)

    # wait = WebDriverWait(context.driver, 10)
    # element = wait.until(EC.(By.CSS_SELECTOR, field))
    # element.clear()
    # time.sleep(1)

    # context.driver.implicitly_wait(10)  # seconds
    # element = context.driver.find_element_by_css_selector(field)
    # element.click()
    # time.sleep(1)
    # size = element.get_attribute('value')
    # for i in size:
    #     element.send_keys(Keys.BACK_SPACE)


# -------------------------------------**** MESSAGE VALIDATION ****--------------------------------------

@when('The user will see a message "{text}" in this Id "{Id}"')
def step_impl(context, text, Id):
    elem = context.driver.find_element_by_id(Id)
    message = context.Valid = elem.get_attribute('innerHTML')
    if len(message) == 0:
        time.sleep(3)
        context.Valid = elem.get_attribute('innerHTML')
        assert_that(context.Valid.strip(), equal_to(text))
    else:
        pass



@when('the user will see a message "{text}" in this CSS "{Css}"')
def step_impl(context, text, Css):
    elem = context.driver.find_element_by_css_selector(Css)
    context.Valid = elem.get_attribute('innerHTML')
    assert_that(context.Valid.strip(), equal_to(text))


@when('the user will see a message "{text}" in this Id "{Id}"')
def step_impl(context, text, Id):
    elem = context.driver.find_element_by_id(Id)
    message = context.Valid = elem.get_attribute('innerHTML')
    if len(message) == 0:
        time.sleep(3)
        context.Valid = elem.get_attribute('innerHTML')
        assert_that(context.Valid.strip(), equal_to(text))
    else:
        pass


@when('the user will see a message "{error_by_field}" in this Css Selector "{Css}" and position "{position}"')
def step_impl(context, error_by_field, Css, position):
    elem = context.driver.find_elements_by_css_selector(Css)
    # print(elem[0].text)
    # print(elem[1].text)
    # print(elem[2].text)
    # print(elem[3].text)
    context.Valid = elem[int(position)].text
    assert_that(context.Valid, equal_to(error_by_field))


@when('the user will see a message "{text}" in this Class "{Class}"')
def step_impl(context, text, Class):
    elem = context.driver.find_element_by_class_name(Class)
    context.Valid = elem.get_attribute('innerHTML')
    assert_that(context.Valid.strip(), equal_to(text))


@when('the user will see a tittle "{text}" in this tag "{tag}"')
def step_impl(context, text, tag):
    elem = context.driver.find_element_by_tag_name(tag)
    context.Valid = elem.get_attribute('innerHTML')
    if str(context.Valid) == text:
        pass
    else:
        breakpoint()
    assert_that(context.Valid, equal_to(text))


@when("save message from position CSS:{Class}")
def step_impl(context, Class):
    elem = context.driver.find_element_by_css_selector(Class)
    context.message_position = elem.get_attribute('innerHTML')


@when("compare two text '{Class}'")
def step_impl(context, Class):
    elem = context.driver.find_element_by_css_selector(Class)
    context.message_compare = elem.get_attribute('innerHTML')
    assert_that(context.message_compare,equal_to(context.message_position))

#==================================
'''     ATTRIBUTE BY CLASS      '''
#==================================

@when('the "{namebtn}" button has the class "{ClassName}"')
def step_impl(context, namebtn, ClassName):
    IOS = context.driver.find_element_by_id(namebtn)
    active = (IOS.get_attribute('class'))
    if active == ClassName:
        pass
    else:
        breakpoint()


#==================================
'''      FIELD AUTOCOMPLETE     '''
#==================================

@when('the field "{FieldName}" was autocomplete')
def seen(context, FieldName):
 field = context.driver.find_element_by_id(FieldName)
 Valuefield = field.get_attribute('value')
 if len(Valuefield) == 0:
     breakpoint()
 else:
     pass


#==================================
'''          GOOGLE TABS        '''
#==================================


@when('The user goes to the tab of "{NameTab}"')
def step_impl(context, NameTab):
 context.driver.switch_to.window(context.driver.window_handles[int(NameTab)])


@when('Open new tab with this URL: {OpenURL}')
def step_impl(context, OpenURL):
 func = f"window.open({OpenURL})"
 # print(func)
 # AssertionError
 context.driver.execute_script(func)


@when('Refresh URL')
def step_impl(context):
 context.driver.refresh()


@when('close chrome')
def step_impl(context):
 context.driver.close()


# #==================================
# '''      SET WINDOW SIZE         '''
# #==================================
#
#
# @when("Window size width:'{width}',heigth:'{heigth}'")
# def step_impl(context, width, heigth):
#    context.driver.set_window_size(int(width), int(heigth))
#

#
# #==================================
# """ Save Screenshot by comparison domains"""
# #==================================
#
#
# @when('Take a Screenshot by image base. save to this folder:"{folder}" with the name "{name}"')
# def step_impl(context, folder, name):
#    context.driver.save_screenshot('features_front/Visual_Regression/base_image/' + folder + '/' + name + '.png')
#
#
# @when('Take a Screenshot by image compare. save to this folder:"{folder}" with the name "{name}"')
# def step_impl(context, folder, name):
#    context.driver.save_screenshot('features_front/Visual_Regression/base_compare/' + folder + '/' + name + '.png')


# #==================================
# '''     TO COMPARE IMAGES       '''
# #==================================
#
#
# @when('To Get the image 1 by xpath:"{image1}", to Get the image 2 by xpath:"{image2}", Compare image 1 and 2. add the difference image to this xpath:"{differenceImage}"')
# def step_impl(context, image1, image2, differenceImage):
#     CompareImg = ("compare " + image1+' ' + image2 +' '+ differenceImage)
#     subprocess.call(CompareImg, shell=True)
#
#     channel = 'screenshot'
#     h1 = Image.open(image1).histogram()
#     h2 = Image.open(image2).histogram()
#     if h1 == h2:
#         pass
#     else:
#         Slack_Report_IMG(channel,differenceImage)




#==================================
'''         CHECK ITEMS       '''
#==================================


@when('Check if the item with Class Name:"{ClassName}" exist')
def step_impl(context, ClassName):
    context.driver.implicitly_wait(10)    # seconds
    element = context.driver.find_element_by_css_selector(ClassName)
    if element is not None:
        pass
    else:
        breakpoint(step_impl)


@when('Check if the item with Class Name:"{ClassName}" not exist')
def step_impl(context, ClassName):
    try:
        context.driver.implicitly_wait(5)  # seconds
        context.driver.find_element_by_css_selector(ClassName)
        print('testing')
    except:
        breakpoint(step_impl)



#====================================================================
#spacial test
@when('check if when you add a video it appears in programs')
def step_impl(context):
    time.sleep(3)
    context.driver.find_element_by_id('menu').click()
    time.sleep(2)
    context.driver.find_element_by_id('menu_internet').click()
    time.sleep(4)
    context.driver.find_element_by_id('search').send_keys('diomedes diaz')
    time.sleep(2)
    context.driver.find_element_by_id('search_button').click()
    time.sleep(5)


    #add new video to my programs
    # compare = context.driver.find_element_by_css_selector('.items-stretch > div:nth-child(2) button').click()
    # time.sleep(2)
    compare = context.driver.find_element_by_css_selector('.items-stretch div:nth-child(2) .q-card__section img:nth-child(1)').click()
    time.sleep(2)

    url = context.driver.find_element_by_css_selector('.items-stretch > div:nth-child(2) iframe').get_attribute('src')
    time.sleep(3)
    context.driver.find_element_by_id('menu').click()
    time.sleep(3)
    click = context.driver.find_element_by_css_selector('.q-list a:nth-child(3)').click()
    time.sleep(2)
    time.sleep(3)
    videos = context.driver.find_elements_by_css_selector('.items-stretch >div > div > div > div:nth-child(2) > iframe')

    list = []
    for video in videos:
        if url in video.get_attribute('src'):
            list.append(video.get_attribute('src'))
            pass
            break
        else:
            pass

    if url in list:
        pass
    else:
        breakpoint()


# @when('Click with random CSS value')
# #Add video in my galery
# def step_impl(context):
#
#     Result = ('.items-stretch div:nth-child('+Data.Add_video_in_my_galery()+') .q-card__section img:nth-child(1)')
#     # context.driver.find_element_by_class_name(str(Result)).click()
#
#     wait = WebDriverWait(context.driver, 10)
#     element = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, Result)))
#     element.click()



@when('add video')
def step_impl(context):
    # context.driver.find_element_by_id('internet_search_btn').click()
    time.sleep(4)
    # context.driver.find_element_by_id('search').send_keys('testing')
    time.sleep(2)
    context.driver.find_element_by_id('search_button').click()
    time.sleep(4)
    #add new video to my programs
    compare = context.driver.find_element_by_css_selector('.items-stretch div:nth-child(2) .q-card__section img:nth-child(1)').click()
    time.sleep(2)
#===================================================================

